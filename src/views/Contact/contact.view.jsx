import React from "react";
import { useMediaQuery } from "react-responsive";

import { Title, SubTitle, ContactForm } from "../../components"

export default function ContactView() {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    return (
        <>
        { !isMobile &&
            <div className="row align-items-center h-85 mx-5">
                <div className="col-8">
                    <Title size="long">Do you want to contact me?</Title>
                    <SubTitle>If you are interested in talk to me about some project or only
                        talk with me you can contact me in my social networks or you can
                        send me an email filling the form below
                    </SubTitle>
                </div>
                <div className="col-4">
                    <ContactForm />
                </div>
            </div>
        }
        { isMobile &&
            <div style={{
                height: '95vh'
            }} className="row align-items-center mx-2">
                <div>
                    <Title size="long">Do you want to contact me?</Title>
                    <SubTitle>If you are interested in talk to me about some project or only
                        talk with me you can contact me in my social networks or you can
                        send me an email filling the form below
                    </SubTitle>
                </div>
                <div>
                    <ContactForm />
                </div>
            </div>
        }
        </>
    )
}