import Home from "./Home/home.view";
import Projects from "./Projects/projects.view";
import Contact from "./Contact/contact.view"
import Skills from "./Skills/skills.view"

export { Home, Projects, Contact, Skills };