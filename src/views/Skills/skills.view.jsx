import React from "react";
import { Title, SkillCard } from "../../components";

import { pl, fr, db, ot } from '../../data/skills';

export default function SkillsView(){

    return (
        <div className="col py-4 align-items-center mx-5">
            <Title size="long">My Skills</Title>
            <SkillCard direction="normal" icons={ pl.icons } title={ pl.title }  description={ pl.description } />
            <SkillCard direction="reverse" icons={ fr.icons } title={ fr.title }  description={ fr.description } />
            <SkillCard direction="normal" icons={ db.icons } title={ db.title }  description={ db.description } />
            <SkillCard direction="reverse" icons={ ot.icons } title={ ot.title }  description={ ot.description } />
        </div>
    )
}
