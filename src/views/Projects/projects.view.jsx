import React from 'react';
import { Title, SubTitle, ProjectCard } from '../../components';


import { projects } from '../../data/projects.js';

export default function Projects() {


    return (
        <div className="col py-4 align-items-center mx-5">
            <Title size="long">My Projects</Title>
            <SubTitle>These are some of the projects I develop for learning, practice or how an asignation</SubTitle>
            {
                projects.map((project, i) => (
                    <ProjectCard 
                        key={i} 
                        title={project.title} 
                        description={project.description} 
                        images={project.images} 
                        link={project.link} 
                        technologies={project.technologies} 
                    />
                ))
            }
        </div>
    )
}