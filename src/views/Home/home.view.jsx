import React from "react";
import { useMediaQuery } from "react-responsive";

import './index.css'

import { SubTitle, Title } from "../../components";

import Avatar from '../../assets/avatar.svg';

export default function HomeView() {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <>
        { !isMobile &&
            <div className="row align-items-center h-85 mx-5">
                <div className="col-9">
                    <Title size="long">Welcome to my portfolio, I'm Luis Martinez</Title>
                    <br/>
                    <SubTitle>I’m a Systems Engineering Intern from Tegucigalpa, Honduras,
                        my preferred programming area is backend development,
                        however I have knowledge about frontend development.</SubTitle>
                </div>
                <div className="col-3">
                    <img src={Avatar} alt="avatar" width='300' height='300' />
                </div>
            </div>
        }
        { isMobile &&
            <div style={{
                width: "100%",
                margin: '15px 0px',
                height: '85vh'
            }}
            className="row align-items-center"
            >
                <div>
                    <Title size="long">Welcome to my portfolio, I'm Luis Martinez</Title>
                    <div>
                        <img src={Avatar} alt="avatar" width='200' height='200' />
                    </div>
                    <div>
                        <br/>
                        <SubTitle>I’m a Systems Engineering Intern from Tegucigalpa, Honduras,
                            my preferred programming area is backend development,
                            however I have knowledge about frontend development.</SubTitle>
                    </div>
                </div>
            </div> 
        }
        </> 
    )

}