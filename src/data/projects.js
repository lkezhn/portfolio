export const projects = [
    {
        'title': 'Placeholder project',
        'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Fringilla phasellus faucibus scelerisque eleifend. Pharetra sit amet aliquam id diam. Eget egestas purus viverra accumsan in nisl nisi scelerisque. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Posuere urna nec tincidunt praesent. Eu ultrices vitae auctor eu. Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Vestibulum lectus mauris ultrices eros in cursus. Et netus et malesuada fames ac turpis. Volutpat maecenas volutpat blandit aliquam etiam erat velit. Blandit massa enim nec dui nunc mattis. ',
        'images': [
            'https://raw.githubusercontent.com/LKezHn/BDIProject/main/docs/img1.png',
            'https://raw.githubusercontent.com/LKezHn/BDIProject/main/docs/img1.png',
        ],
        'technologies': [
            'React', 'Express', 'NodeJs',
        ],
        'link': 'https://google.hn'
    },
    {
        'title': 'Placeholder project',
        'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Fringilla phasellus faucibus scelerisque eleifend. Pharetra sit amet aliquam id diam. Eget egestas purus viverra accumsan in nisl nisi scelerisque. Nibh sit amet commodo nulla facilisi nullam vehicula ipsum. Posuere urna nec tincidunt praesent. Eu ultrices vitae auctor eu. Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus. Interdum varius sit amet mattis vulputate enim nulla aliquet porttitor. Vestibulum lectus mauris ultrices eros in cursus. Et netus et malesuada fames ac turpis. Volutpat maecenas volutpat blandit aliquam etiam erat velit. Blandit massa enim nec dui nunc mattis. ',
        'images': [
            'https://raw.githubusercontent.com/LKezHn/POOProject/main/docs/img1.png'
        ],
        'technologies': [
            'Python3', 'Flask', 'MySQL',
        ],
        'link': 'un link cualquiera'
    },

]

