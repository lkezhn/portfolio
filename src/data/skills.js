/** Programming Langauges */
import python from '../assets/python.svg'
import nodejs from '../assets/node-js.svg'
import typescript from '../assets/typescript.svg'
import php from '../assets/php.svg'

/** Frameworks  */
import express from "../assets/express.svg"
import react from "../assets/react.svg"
import angular from "../assets/angular.svg"
import jqwidgets from "../assets/jqwidgets.svg"

/** Databases */
import mysql from '../assets/mysql.svg'
import mongo from '../assets/mongodb.svg'

/** Other tools */
import linux from '../assets/linux-ubuntu.svg'
import css from '../assets/css.svg'
import html from '../assets/html.svg'
import git from '../assets/git.svg'

const programmingLanguages = {
    "title": "Lenguajes de Programación",
    'description': "Tengo conocimientos en lenguajes como Javascript, Python, Typescript y PHP, éstos son los lenguajes con los que más he trabajado, aunque tambien he trabajado en menor medida con lenguajes como Java y Golang",
    "icons": [
        python, 
        nodejs, 
        typescript,
        php
    ]
}

const frameworks = {
    "title": "Frameworks",
    "description": "En el backend he trabajado en su mayoria con Expressjs, tambien he podido trabajar con Fiber, framework de Golang pero en menos medida y también con lenguajes puros como PHP, hablando de frontend he trabajado con frameworks conocidos como Angular y React, asi tambien con frameworks menos populares como lo es JQWidgets, también he trabajado con frameworks CSS como Bootstrap y TailwindCSS",
    "icons": [
        express,
        react,
        angular,
        jqwidgets
    ]
}

const databases = {
    "title": "Bases de Datos",
    "description": "Hablando de Bases de Datos Relacionales he trabajado y poseo mas experiencia trabajando con MySQL, también tuve la oportunidad de trabajar con OracleDB pero en un tiempo menor, con Bases de Datos No Relacionales he utilizado MongoDB y Firebase, teniendo mas experiencia utiilzando MongoDB",
    "icons": [
        mysql,
        mongo
    ]
}

const other_tools = {
    "title": "Otras Herramientas",
    "description": "También he utilizado otras herramientas, como git para el control de versiones, HTML5 Y CSS3 para sitios web, de igual manera sistemas operativos Linux por lo que estoy familiarizado con la linea de comandos, tambien he utilizado herramientas como Figma, Draw.io, Canva",
    "icons": [
        linux,
        git,
        html,
        css
    ]
}

export { programmingLanguages as pl, frameworks as fr, databases as db, other_tools as ot }