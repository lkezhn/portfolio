import { Route, Routes } from 'react-router-dom';

import './App.css';
import { Footer, NavBar } from './components';
import { Home, Projects, Contact, Skills } from './views';

function App() {
  return (
    <div className="App h-100">
      <NavBar />
      <Routes>
        <Route path='/' exact element={<Home />} />
        <Route path='/home' element={<Home />} />
        <Route path='/projects' element={<Projects />} />
        <Route path='/contact' element={<Contact />} />
        <Route path='/skills' element={<Skills />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
