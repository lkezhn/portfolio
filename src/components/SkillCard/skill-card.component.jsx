import React from "react";
import { useMediaQuery } from "react-responsive";

import Card from "../Card/card.component";
import Description from "../Description/description.component";
import Title from "../Title/title.component";
import Carousel from "../Carousel/carousel.component";


export default function SkillCard({ title, description, icons, direction }) {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <Card shadow="reverse">
            { !isMobile &&
                <div className="row mx-5 py-3">
                    {
                        direction === "normal" &&
                        <>
                            <div className="col-5"><Carousel controls={false} images={icons} /></div>
                            <div className="col-7">
                                <Title size="short">{title}</Title>
                                <Description>{description}</Description>
                            </div>
                        </>
                    }
                    {
                        direction === "reverse" &&
                        <>
                            <div className="col-7">
                                <Title size="short">{title}</Title>
                                <Description>{description}</Description>
                            </div>
                            <div className="col-5"><Carousel controls={false} images={icons} /></div>
                        </>
                    }
                </div>
            }
            { isMobile &&
                <div className="row mx-1 py-2">
                    {
                        direction === "normal" &&
                        <>
                            <Title size="short">{title}</Title>
                            <div ><Carousel controls={false} images={icons} /></div>
                            <div >
                                <Description>{description}</Description>
                            </div>
                        </>
                    }
                    {
                        direction === "reverse" &&
                        <>
                            <Title size="short">{title}</Title>
                            <div><Carousel controls={false} images={icons} /></div>
                            <div>
                                <Description>{description}</Description>
                            </div>
                        </>
                    }
                </div>

            }
        </Card>
    )
}