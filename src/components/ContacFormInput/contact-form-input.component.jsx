import React from "react";

import './index.css'

const ContactFormInput = (props) => {

    return (
        <>
            <div className="mx-4 my-1">
                <label className="input-label" htmlFor={props.name}>{props.label}</label><br />
            </div>
            {
                props.type === "input" && props.name !== 'Email' && <input type="text" required className="input" name={props.name.toLowerCase()} id={props.name.toLowerCase()} />
            }
            {
                props.type === "input" && props.name === 'Email' && <input type="email" required className="input" name={props.name.toLowerCase()} id={props.name.toLowerCase()} />
            }
            {
                props.type === "textarea" && <textarea rows='5' required className="input" style={{ "resize": 'none' }} name={props.name.toLowerCase()} id={props.name.toLowerCase()}></textarea>
            }
            <br />
        </>
    )

}

export default ContactFormInput;