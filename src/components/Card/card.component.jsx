import React from "react";
import "./index.css"

export default function Card({ shadow, children }) {

    return (
        <>
            { shadow === "normal" &&
                <div className='mx-2 mt-4 mb-5 px-2 rounded card normal-shadow'>
                    {children}
                </div>
            }
            { shadow === "reverse" &&
                <div className='mx-2 mt-4 mb-5 px-2 rounded card reverse-shadow'>
                    {children}
                </div>
            }
        </>
    )

}