import React from "react";
import { useMediaQuery } from "react-responsive";
import AliceCarousel from 'react-alice-carousel';

import 'react-alice-carousel/lib/alice-carousel.css'
import "./index.css"


const responsiveWithControls = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 1 },
};

const responsiveWithoutControls = {
    0: { items: 1 },
    568: { items: 1 },
    1024: { items: 1 },
};

export default function Carousel({ controls, images }) {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    return (
        <>
        {
            !isMobile && controls === true && <div className='col-6 align-items-center py-4'>
                <AliceCarousel
                    mouseTracking
                    //autoHeight
                    responsive={responsiveWithControls}
                    controlsStrategy="alternate"
                >
                    {images.map((image, i) => (
                        <div className='sliderimg-container'>
                            <img src={image} className='sliderimg' key={i}></img>
                        </div>
                    ))}
                </AliceCarousel>
            </div>
        }
        {
            isMobile && controls === true && <div className='col-12 align-items-center py-2'>
                <AliceCarousel
                    mouseTracking
                    disableButtonsControls
                    //autoHeight
                    responsive={responsiveWithControls}
                    controlsStrategy="alternate"
                >
                    {images.map((image, i) => (
                        <div className='sliderimg-container'>
                            <img src={image} className='sliderimg' key={i}></img>
                        </div>
                    ))}
                </AliceCarousel>
            </div>
        }
        {
            controls === false && <div className='col-12 align-items-center py-4'>
                <AliceCarousel
                    autoPlay
                    autoPlayStrategy="none"
                    disableButtonsControls
                    disableDotsControls
                    infinite
                    animationDuration={1200}
                    //autoHeight
                    responsive={responsiveWithoutControls}
                    controlsStrategy="alternate"
                >
                    {images.map((image, i) => (
                        <div className='sliderimg-container'>
                            <img src={image} className='slidericon' key={i}></img>
                        </div>
                    ))}
                </AliceCarousel>
            </div>

        }
        </>
    )

}