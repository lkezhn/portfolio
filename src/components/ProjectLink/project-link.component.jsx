import React from "react";
import { useMediaQuery } from "react-responsive";

import './index.css'

export default function ProjectLink({info}){

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    return (
        <>
        { !isMobile &&
            <div className='d-flex justify-content-end px-2'>
                <a className='project-link' href={info}>Show More</a>
            </div>
        }
        { isMobile &&
            <div className='d-flex justify-content-end px-2 pb-2'>
                <a className='project-link' href={info}>Show More</a>
            </div>
        }
        </>
    )
}