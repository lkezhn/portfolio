import React from "react";
import { useMediaQuery } from "react-responsive";

import './index.css'

export default function Footer() {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <>
        { !isMobile && 
            <footer className='mx-5 h-auto my-2'>
                <div className='row justify-content-between'>
                    <div className='col-6 d-flex'>
                        <a href='https://www.linkedin.com/in/lkezhn'><i className='footer-icon fab fa-2x fa-linkedin mx-2'></i></a>
                        <a href="https://t.me/lkez_hn"><i className='footer-icon fab fa-2x fa-telegram-plane mx-2'></i></a>
                        <a href="https://github.com/LKezHn"><i className='footer-icon fab fa-2x fa-github mx-2'></i></a>
                    </div>
                    <div className='col-6 d-flex justify-content-end'>
                        <span className='footer-secondary-text'>©</span>&nbsp;
                        <span className='footer-primary-text'>{ new Date().getFullYear() }</span>&nbsp; 
                        <span className='footer-secondary-text'>Designed by</span>&nbsp; 
                        <span className='footer-primary-text'>Luis Martinez</span>
                    </div>
                </div>
            </footer>
        }
        { isMobile &&
            <footer className='mx-4 h-auto my-2 mb-3'>
                <div className='row justify-content-between'>
                    <div className='col-5 d-flex'>
                        <a href='https://www.linkedin.com/in/lkezhn'><i className='footer-icon-r fab fa-linkedin mx-2'></i></a>
                        <a href="https://t.me/lkez_hn"><i className='footer-icon-r fab fa-telegram-plane mx-2'></i></a>
                        <a href="https://github.com/LKezHn"><i className='footer-icon-r fab fa-github mx-2'></i></a>
                    </div>
                    <div className='col-7 d-flex justify-content-end align-items-center'>
                        <span className='footer-r-secondary-text'>©</span>&nbsp;
                        <span className='footer-r-primary-text'>{ new Date().getFullYear() }</span>&nbsp; 
                        <span className='footer-r-secondary-text'>Designed by</span>&nbsp; 
                        <span className='footer-r-primary-text'>Luis Martinez</span>
                    </div>
                </div>
            </footer>
        }
        </>
    )

}