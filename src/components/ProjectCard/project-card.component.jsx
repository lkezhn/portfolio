import React from 'react';
import { useMediaQuery } from "react-responsive";

import Carousel from '../Carousel/carousel.component';

import Card from '../Card/card.component';
import Title from "../Title/title.component";
import Description from '../Description/description.component';
import ProjectLink from '../ProjectLink/project-link.component';
import ProjectTechs from '../ProjectTechs/project-techs.component';

import './index.css';


export default function ProjectCard({ title, description, images, technologies, link }) {
    
    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    return (
        <Card shadow="normal">
            { !isMobile &&
                <div className='row'>
                    <Carousel images={images} controls={true} />
                    <div className='col-6 mt-4 align-items-center mr-1'>
                        <Title size="medium">{title}</Title>
                        <Description>{description}</Description>
                        <ProjectTechs technologies={technologies} />
                        <ProjectLink info={link} />
                    </div>
                </div>
            }
            { isMobile &&
                <div className='row'>
                    <Title size="medium">{title}</Title>
                    <Carousel images={images} controls={true} />
                    <div>
                        <Description>{description}</Description>
                        <ProjectTechs technologies={technologies} />
                        <ProjectLink info={link} />
                    </div>
                </div>
            }
        </Card>
    )
}