import NavBar from "./NavBar/navbar.component";
import Title from "./Title/title.component";
import SubTitle from "./SubTitle/subtitle.component";
import Footer from "./Footer/footer.component";
import ProjectCard from "./ProjectCard/project-card.component";
import ContactForm from "./ContactForm/contact-form.component";
import SkillCard from "./SkillCard/skill-card.component";

export { NavBar, Title, SubTitle, Footer, ProjectCard, ContactForm, SkillCard };
    