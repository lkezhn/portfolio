import React, { useRef } from "react";
import { useMediaQuery } from "react-responsive";
import { Toaster, toast } from 'react-hot-toast'

import emailjs from '@emailjs/browser'

import Input from "../ContacFormInput/contact-form-input.component";
import "./index.css"
import Card from "../Card/card.component";

export default function ContactForm() {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    const formRef = useRef()

    const sendEmail = (e) => {
        e.preventDefault();

        const emailPromise = emailjs.sendForm("service_sfkwwqp", "template_dbpj32g", formRef.current, "user_zGU290Kajp3EtjEcEepwI")

        toast.promise(emailPromise, {
            loading: "Sending message...",
            success: "Message sended ",
            error: "An error ocurred when sending message"
        })

        formRef.current.reset()
    }

    return (
        <Card shadow="reverse">
            { !isMobile &&
                <div className="container my-3">
                    <form ref={formRef} onSubmit={sendEmail}>
                        <Input type="input" label="Name" name="from_name" />
                        <Input type="input" label="Email" name="email_name" />
                        <Input type="textarea" label="Message" name="message" />
                        <button className="btn-contact mt-2" type="submit">Send Email</button>
                    </form>
                    <Toaster toastOptions={{
                        duration: 3000,
                        style: {
                            background: '#2c2c2c',
                            color: '#A6A8AA',
                        }
                    }} />
                </div>
            }
            { isMobile &&
                <div className="container my-3">
                    <form ref={formRef} onSubmit={sendEmail}>
                        <Input type="input" label="Name" name="from_name" />
                        <Input type="input" label="Email" name="email_name" />
                        <Input type="textarea" label="Message" name="message" />
                        <button className="btn-contact mt-2" type="submit">Send Email</button>
                    </form>
                    <Toaster toastOptions={{
                        duration: 3000,
                        style: {
                            background: '#2c2c2c',
                            color: '#A6A8AA',
                        }
                    }} />
                </div>
            }
        </Card>
    )

}