import React from "react";
import { useMediaQuery } from "react-responsive";

import './index.css'

export default function ProjectTechs({ technologies }) {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' });

    return (
        <>
        { !isMobile &&
            <div className='d-flex justify-content-center flex-wrap'>
                {technologies.map((tech, i) => (
                    <span key={i} className='tech-item m-2 py-1 px-3 rounded'>{tech}</span>
                ))}
            </div>
        }
        { isMobile &&
            <div className='d-flex justify-content-around flex-wrap'>
                {technologies.map((tech, i) => (
                    <span key={i} className='tech-item m-2 py-1 px-2 rounded'>{tech}</span>
                ))}
            </div>
        }
        </>
    )

}