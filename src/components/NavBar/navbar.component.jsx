import React, { useRef, useEffect } from 'react';
import { useLocation} from 'react-router-dom';
import { useMediaQuery } from "react-responsive";

import AnchorTag from '../AnchorTag/anchortag.component';

import './index.css'

import Tags from '../../assets/tags.svg'

export default function NavBar() {
    
    const navLinksRef = useRef(null);
    const location = useLocation();

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    useEffect(() => {
            const links = navLinksRef.current.querySelectorAll('a')
            links.forEach(element => {
                if(location.pathname === '/'  &&  element.innerHTML === 'Home'){
                    element.classList.add("nav-link-active");
                }else if(element.href === window.location.href) {
                    element.classList.add("nav-link-active");
                }
            });
    }, [location.pathname])


    const links = [
        { hrer: '/home', linkName: 'Home', styles: 'nav-item nav-link px-3 mx-1 py-1' },
        { hrer: '/skills', linkName: 'Skills', styles: 'nav-item nav-link px-3 mx-1 py-1' },
        { hrer: '/projects', linkName: 'Projects', styles: 'nav-item nav-link px-3 mx-1 py-1' },
        { hrer: '/contact', linkName: 'Contact', styles: 'nav-item nav-link px-3 mx-1 py-1' },
    ]
 
    const r_links = [
        { hrer: '/home', linkName: 'Home', styles: 'nav-item nav-link px-2 mx-1 py-1' },
        { hrer: '/skills', linkName: 'Skills', styles: 'nav-item nav-link px-2 mx-1 py-1' },
        { hrer: '/projects', linkName: 'Projects', styles: 'nav-item nav-link px-2 mx-1 py-1' },
        { hrer: '/contact', linkName: 'Contact', styles: 'nav-item nav-link px-2 mx-1 py-1' },
    ]

    const changeLinkStyle = (e) => {
        const links = navLinksRef.current.querySelectorAll('a');
        links.forEach(element => {
            element.classList.remove("nav-link-active")
            if (element.innerHTML === e.target.innerHTML) {
                element.classList.add("nav-link-active");
            }
        });
    }

    return (
        <>
        { !isMobile && (
            <nav className="navbar navbar-expand-lg px-5">
                <img src={Tags} width="30" height="30" alt="" />
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav" ref={navLinksRef}>
                        {links.map((link, i) => (
                            <AnchorTag key={i} click={changeLinkStyle} classes={link.styles} href={link.hrer}>{link.linkName}</AnchorTag>
                        ))}
                    </div>
                </div>
            </nav>
        )}
        { isMobile &&
            <nav className="navbar-r navbar-expand-lg pt-3">
                { /**<img src={Tags} width="33" height="33" alt="tags" /> */}
                <div className="navbar-r-nav" ref={navLinksRef}>
                    {r_links.map((link, i) => (
                        <AnchorTag key={i} click={changeLinkStyle} classes={link.styles} href={link.hrer}>{link.linkName}</AnchorTag>
                    ))}
                </div>
            </nav>
        }
        </>
    )
}