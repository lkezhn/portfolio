import React from 'react';
import { useMediaQuery } from "react-responsive";

import './index.css'

export default function SubTitle({children}){

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <>
        { !isMobile && <h3 className='subtitle'>{children}</h3>}
        { isMobile && <h3 className='subtitle-r'>{children}</h3>}
        </>
    )
}