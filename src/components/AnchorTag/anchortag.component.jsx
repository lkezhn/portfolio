import React, { useState } from 'react';
import { Link } from 'react-router-dom';

export default function AnchorTag({ classes, href, children, click}) {

    const [styles, setStyles] = useState(classes)


    return (
        <>
             <Link className={styles} to={href} onClick={click}>{children}</Link>
        </>
    )

}