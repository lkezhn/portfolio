import React from "react";
import { useMediaQuery } from "react-responsive";

import './index.css'

export default function Description({children}){

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <>
        { !isMobile && <h5 className='description pr-2'>{children}</h5> }
        { isMobile && <h5 className='description-r pr-2'>{children}</h5> }
        </>
    )
}