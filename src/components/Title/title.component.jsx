import React from 'react';
import { useMediaQuery } from "react-responsive";

import './index.css';

export default function Title({ size, children }) {

    const isMobile = useMediaQuery({ query: '(max-width: 768px)' })

    return (
        <> 
            { !isMobile && size === "long" && <h1 className='long-title'>{children}</h1> }
            { !isMobile && size === "medium" && <h2 className='medium-title'>{children}</h2> }
            { !isMobile && size === "short" && <h4 className='short-title'>{children}</h4> }

            { isMobile && size === "long" && <h1 className='long-r-title'>{children}</h1> }
            { isMobile && size === "medium" && <h2 className='medium-r-title'>{children}</h2> }
            { isMobile && size === "short" && <h4 className='short-r-title'>{children}</h4> }

        </>
    )
}